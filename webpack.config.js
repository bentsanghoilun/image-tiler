const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');
require('dotenv').config({ path: './.env' });


module.exports = {
    entry: ["regenerator-runtime/runtime.js", './src/index.js'],
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: `${process.env.library_name}.min.js`,
        library: process.env.library_name,
        libraryTarget: "var"
    },
    devServer: {
        static: {
            directory: path.join(__dirname, 'public'),
        },
        compress: true,
        port: 9000,
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'Development',
            filename: 'index.html',
            template: 'src/index.html',
            libraryPath: `${process.env.library_name}.min.js`,
            inject: false
        }),
        new webpack.DefinePlugin({
            "process.env": JSON.stringify(process.env)
        }),
    ],
    module: {
        rules: [
            {
                test: /\.(js)$/,
                exclude: /node_modules/,
                use: "babel-loader",
            },
            {
                test: /\.css$/i,
                use: ["style-loader", "css-loader"],
            },
        ],
    },
    mode: "production",
    optimization: {
        minimize: true,
    },
};