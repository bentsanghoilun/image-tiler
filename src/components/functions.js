const addCss = async () => {
    var styleElement = document.createElement("style");
    var cssText = `
        .sg-imgTile-container {
            display: inline-flex;
            flex-direction: row;
            align-items: flex-start;
            flex-wrap: wrap;
            width: 100%;
            position: relative;
        }
        .sg-imgTile-tile{
            display: flex;
            overflow: hidden;
            align-items: center;
            justify-content: center;
            position: relative;
        }
        .sg-imgTile-img {
            max-width: unset !important;
            min-width: unset !important;
            height: auto;
            transition-property: opacity;
            transition-duration: 1s;
            position: absolute;
            opacity: 0;
        }
        .sg-imgTile-img-holder{
            width: 100%;
            height: auto;
            position: relative;
            opacity: 0;
        }
    `;
    styleElement.appendChild(document.createTextNode(cssText));
    document.body.append(styleElement);
    return true;
}

const GetElement = async () => {
    const target = document.getElementById('imageTiler');
    if (!target) {
        console.error('StreamGo > Image Tiler: no element found with the id "imageTiler".');
        return
    }
    if (!target.dataset.sources) {
        console.error('StreamGo > Image Tiler: no image sources found.');
        return
    }
    const targetObj = {
        element: target,
        rows: parseInt(target.dataset.rows) || 10,
        columns: parseInt(target.dataset.columns) || 10,
        sources: JSON.parse(target.dataset.sources).sources
    }
    return targetObj;
}

const CreateWall = async (targetObj) => {
    var wallDiv = document.createElement('div');
    wallDiv.classList.add('sg-imgTile-container');
    wallDiv.id = 'sg-imgTile-container';
    const maxTiles = targetObj.rows * targetObj.columns;
    console.log(targetObj.rows, targetObj.columns, maxTiles);
    const targetWidthRatio = targetObj.columns;

    // for demo only
    window.sg = new Object;
    window.sg.imgSrcs = targetObj.sources;

    for (var i = 0; i < maxTiles; i++){
        const tile = await addTile(targetWidthRatio, i);
        wallDiv.appendChild(tile);
    }

    targetObj.element.appendChild(wallDiv);
}

const addTile = async (targetWidthRatio) => {
    var tile = document.createElement('div');
    tile.classList.add('sg-imgTile-tile');
    tile.style.width = `${100 / targetWidthRatio}%`;
    tile.style.height = `auto`;
    tile.style.backgroundColor = `rgba(0,0,0,${Math.random() / 2})`;

    var imgHolder = document.createElement('img');
    imgHolder.classList.add('sg-imgTile-img-holder');
    imgHolder.src = `https://sg-template-assets.s3.eu-west-2.amazonaws.com/common-scripts/image-tiler/img-holder.jpg`;
    tile.appendChild(imgHolder);

    var imgs = {
        imgA: document.createElement('img'),
        imgB: document.createElement('img')
    }
    for (const [key, value] of Object.entries(imgs)) {
        value.classList.add('sg-imgTile-img');
        value.style.width = `${1000}%`;
        if (key === 'imgA') {
            value.classList.add('img-a');
        } else {
            value.classList.add('img-b');
        }
        value.addEventListener('load', (e) => {
            e.target.style.opacity = 1;
            console.log(e.target.offsetWidth);
        })
        tile.appendChild(value);
    }

    changeImg(tile);
    
    return tile;
}

const changeImg = (tile, alternative = false) => {
    const page = Math.floor(Math.random() * window.sg.imgSrcs.length);
    const newX = Math.floor(Math.random() * 10), newY = Math.floor(Math.random() * 10);

    var newSrc = window.sg.imgSrcs[page];
    
    var img = alternative ? tile.querySelector('.img-a') : tile.querySelector('.img-b');
    var altImg = alternative ? tile.querySelector('.img-b') : tile.querySelector('.img-a');
    img.src = newSrc;
    img.style.top = `-${newX * 100}%`;
    img.style.left = `-${newY * 100}%`;
    // img.style.opacity = 1;
    img.style.zIndex = 9;

    altImg.style.zIndex = 0;
    altImg.style.opacity = 0;

    setTimeout(() => {
        changeImg(tile, !alternative);
    }, Math.floor(Math.random() * 30000));
}

export { GetElement, CreateWall, addCss };