import 'regenerator-runtime/runtime';
import { GetElement, CreateWall, addCss } from './components/functions';

window.addEventListener('load', async () => {
    const targetObj = await GetElement();
    if (!targetObj.element) {
        return
    }
    await addCss();
    console.log(targetObj);
    CreateWall(targetObj);
});