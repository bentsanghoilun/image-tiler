# Widget Boilerplate

## Setup
Ensure to change the 'library_name' var in the .env
```
library_name=YOUR_WIDGET_NAME
```
When complied the paths and file names will follow this Library name.

Make sure to do `npm install` before running or building.

### Local Run
```
npm start
```

### Build for Production
```
npm run build
```